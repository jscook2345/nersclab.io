# WPS

## Build WPS 

### Required libraries

WPS (specifically grib.exe) uses three external libraries: JasPer, zlib, and PNG (libpng). 
For most users using netCDF for file I/O, the netCDF libraries are also required; while building
WPS, environmental variable NETCDF needs to be set to the path of the NetCDF installation.
We can use the environmental variable $NETCDF_DIR set by the cray-netcdf module:

```
export NETCDF=$NETCDF_DIR
``` 

On Perlmutter,  zlib and libpng are available in /usr, so the configure tool can find them
automatically. JasPer is not currently available in a standard path nor as a module, so a user 
needs to install it from the downloaded source code (example script below). 

#### Example build script for the JasPer external library

```bash

#!/bin/bash
#build the three libraries used by WPS's ungrib.exe
#the script is based on  the direction here:
#http://www2.mmm.ucar.edu/wrf/OnLineTutorial/compilation_tutorial.php

#on Perlmutter, zlib and libpng are available in /usr, 
#so configure should find them
#jaspser can be compiled by this script, or 
#one could use the one installed under the Spack module/environment
#%module load cpu
#%module load spack
#%spack load jasper
#(will be tested)

imach="pm" #system name, pm for Perlmutter

# WRF/WPS directories#the script is based on  the direction here:
#http://www2.mmm.ucar.edu/wrf/OnLineTutorial/compilation_tutorial.php

#on Perlmutter, zlib and libpng are available in /usr, 
#so configure should find them
#jaspser can be compiled by this script, or 
#one could use the one installed under the Spack module/environment
#%module load cpu
#%module load spack
#%spack load jasper
#(will be tested)

imach="pm" #system name, pm for perlmutter

# WRF/WPS directories, using the WRF-SIG directory as example
wrfroot=/global/cfs/cdirs/m4232/model/pm/v4.4

export WRF_DIR=${wrfroot}/WRF
export WPS_DIR=${wrfroot}/WPS

#Load the same modules used for WRF, saved in a bash script ---------------------------
loading_script="/global/cfs/cdirs/m4232/scripts/build/load_modules_2022-12_wrfsig.sh"
source ${loading_script} ${imach}


#directory for the external libraries, here using a global common for WRF-SIG
export EXDIR=/global/common/software/m4232/test/LIBRARIES
mkdir -p ${EXDIR}

libcodedir=/global/cfs/cdirs/m4232/model/external #where source code of external library is downloaded 

#compiler wrappers fail when the configure script check for cross-compilation
#so use the base compiler to build the external library

export CC=gcc
export CXX=g++
export FC=gfortran
export F77=gfortran

echo "installing JasPer"

cd ${libcodedir}/jasper-1.900.1
./configure --prefix=${EXDIR}/grib2
make
make install

```

On Cori, all the three libraries are available as modules. So simply run the following commands 
when building WPS:

```
module load zlib
module load png
module load jasper

```

### Compile WPS

To build the three WPS programs: geogrid, ungrib, and metgrid, we first run the configure script 
in the top WPS directory. Configure collects system information and also asks a user to 
select a model configuration. For a standard use-case of running WPS on a log-in node, 
option #1 (in WPS v4.4) is appropriate for Perlmutter,    

```
 1.  Linux x86_64, gfortran    (serial)
```

 and option #37

```
37 Cray XC CLE/Linux x86_64, Intel compiler   (serial)
```

works on Cori Haswell.

After the system information and WPS configuration are saved in configure.wps, we use the "compile"
 script in the top WPS directory to compile the three WPS programs. The compile script has to be 
 run while loading the same modules (libraries) as those used to build WRF, because WPS uses WRF's 
 I/O libraries. See below for an example script for Perlmutter.

!!! note
    If WRF was compiled with the dm+sm option, then we need to add a compiler flag 
    related to shared-memory library (-lgomp) to "WRF_LIB" in configure.wps 
    (see this [WRF forum post]). 

[WRF forum post]: https://forum.mmm.ucar.edu/threads/resolved-wrf-dm-sm-referencia-a-__kmpc_dispatch_next_4-sin-definir.927/ 

```bash

#!/bin/bash
set -e
set -o pipefail

#based on  the direction here
#http://www2.mmm.ucar.edu/wrf/OnLineTutorial/compilation_tutorial.php

imach="pm" #system name, pm for perlmutter

config_wps=true   #set true to run configure script

build_wps=true   #set true to compile WPS

# WRF directories, taken from WRF-SIG project directory (only open to WRF SIG members)
mversion="v4.4"
wrfroot="/global/cfs/cdirs/m4232/model"
script_dir="/global/cfs/cdirs/m4232/scripts/build"

export WRF_DIR=${wrfroot}/${imach}/${mversion}/WRF
export WPS_DIR=${wrfroot}/${imach}/${mversion}/WPS

#Modules --------------------------------------------------------------------
modversion="2022-12"  #denotes the time of major system update
loading_script="${script_dir}/load_modules_${modversion}_wrfsig.sh"
source ${loading_script} ${imach}


##capture starting time for log file name
idate=$(date "+%Y-%m-%d-%H%M")
echo $idate

#directory for the external libraries are built
#only jasper needs to be installed on Perlmutter

export WRF_EXTERNAL_LIBS=${wrfroot}/${imach}/LIBRARIES

#compiler wrappers
export CC=CC
export CXX=cc
export FC=ftn
export F77=ftn

#base compilers also work for running a quick serial task on a log-in node 
#export CC=gcc
#export CXX=g++
#export FC=gfortran
#export F77=gfortran

export FCFLAGS="-m64" 
export FFLAGS="-m64"

#from the netcdf module
export NETCDF=$NETCDF_DIR
 
#jasper path to be used for COMPRESSION_LIBS and COMPRESSION_INC
#environment variables used by the configure script
#local installation
export JASPERLIB="$WRF_EXTERNAL_LIBS/grib2/lib"
export JASPERINC="$WRF_EXTERNAL_LIBS/grib2/include"
 

########### build WPS; requires WRF being built first to use the WRF IO routines
if [ "$config_wps" = true ]; then

    echo "Configuring WPS"

    cd ${WPS_DIR}
    pwd

    ./clean -a

    ./configure SCC=gcc SFC=gfortran  
    #choose option 1 to run  WPS on a log-in node as a serial executable 
    #   1.  Linux x86_64, gfortran    (serial)
    
    #if using compute node with MPI, then 
    #./configure SCC=cc SFC=ftn
    #   3.  Linux x86_64, gfortran    (dmpar) 
    
    #if WRF was compiled with the dm+sm option, then we 
    #need to add a compiler flag related to shared-memory library to "WRF_LIB" in configure.wps
    #based on a WRF forum post
    #https://forum.mmm.ucar.edu/threads/resolved-wrf-dm-sm-referencia-a-__kmpc_dispatch_next_4-sin-definir.927/
    sed -i 's/lnetcdff/lnetcdff -lgomp/' configure.wps

fi

if [ "$build_wps" = true ]; then

    echo "Building WPS"

    cd ${WPS_DIR}
    pwd
    #here saving the compile log in the specified directory for record
    bldlog=${script_dir}/compile_logs/compile_wps_${idate}_${imach}.log
    echo  "compile log file is ${bldlog}"
        
    ./compile &> ${bldlog}
fi

```
