# Perlmutter Scratch

Permutter Scratch is an all-flash Lustre file system designed for high
performance temporary storage of large files. It is intended to
support intensive I/O for jobs that are being actively computed on the
Perlmutter system. We recommend that you run your jobs, especially
data intensive ones, from the Perlmutter Scratch File System.

## Usage

The Perlmutter Scratch File system should always be referenced using
the environment variable `$PSCRATCH`. This variable expands to
`/pscratch/sd/FirstLetterOfUserName/YourUserName`. For example,
for user, `elvis`, `$PSCRATCH` would become
`/pscratch/sd/e/elivs`. Perlmutter scratch is available to all Permutter
compute nodes and is tuned for high performance.

## Quotas

If your scratch usage exceeds your quota, you will not be able to
write to the file system until you reduce your usage.

## Performance

The Perlmutter Scratch File System is an all-flash file system. It has
35 PB of disk space, an aggregate bandwidth of >5 TB/sec, and 4
million IOPS (4 KiB random). It has 16 MDS (metadata servers), 274 I/O
servers called OSSs, and 3,792 dual-ported NVMe SSDs.

### Default File Striping

By default files on Perlmutter are striped across a single OST. Given
the large number of small files on our system, this is a compromise to
ensure reasonable performance for most files. However, some I/O
patterns may find better performance with different striping. Please
see our [Lustre striping page](../performance/io/lustre/index.md) for
a longer discussion.

## Backup

All NERSC users should backup important files on a regular
basis. Ultimately, it is the user's responsibility to prevent data
loss.

!!! warning
    The scratch file system is subject to purging. Please make sure to
    back up your important files (e.g. to [HPSS](archive.md)).

## Lifetime

Perlmutter scratch directories are [purged](quotas.md). Perlmutter
scratch directories may be deleted after a user is no longer active.
