# Using the Rancher CLI

In addition to the Rancher GUI, your software in Spin can also be managed via
the Rancher CLI, using a tool called `rancher`. You might already be familiar
with the native Kubernetes CLI tool `kubectl`, which is actually used by the
`rancher kubectl` subcommand. However, `kubectl` by itself will not work with
Spin due to security and Role Based Access Controls (RBAC) built into Rancher.

!!! Tip 
    These CLI tools are general purpose tools and have options which cannot be
    used with Spin. If you try these options, you should see an error message
    such as "Access Denied".

## Accessing the tool

There are two ways to use the Rancher CLI: From a NERSC login node such as
**cori.nersc.gov** & **spin.nersc.gov**, or from your laptop.

Either way, you will need to obtain an API key from the Rancher GUI and store
it for use with the CLI.

### Using the CLI from cori/spin.nersc.gov

Log into a login node, and load the Spin 2 module:

```
stefanl@spin:~ $ module load spin/2.0
stefanl@spin:~ $ which rancher
/global/common/shared/isg/spin/2.0/bin/rancher
stefanl@spin:~ $ rancher --version
rancher version v2.4.3
stefanl@spin:~ $ 
```

Then, proceed with obtaining your API credentials below.

### Using the CLI from your laptop

You will need:

1. An operating system with a good command-line terminal, such as macOS, Linux,
   or Windows 10 with WSL2.
1. An account on Spin.
1. The `kubectl` CLI. This is normally provided by Docker for Mac and Docker
   for Windows, or from the standard package management system on Linux.
1. The `rancher` CLI. This can be downloaded by logging into
   https://rancher2.spin.nersc.gov and download the CLI from the bottom right
   corner.

Both `rancher` & `kubectl` must be in your `$PATH`.

!!! important "Warning about versions in Docker for Mac, Docker for Windows, Linux & your self-maintained verisons"

    If you use *Docker for Mac*, vendor packages, Ubuntu Snap, etc. Keep an eye
    on the version of `kubectl` provided by these vendors-- `kubectl` may get
    updated without your knowledge, which can lead to incompatibilities.

    The `kubectl` CLI must be within [one minor version of the Kubernetes server
    version](https://kubernetes.io/docs/setup/release/version-skew-policy/#kubectl).
    That is, if the Kubernetes cluster runs 1.17.x, your CLI must be version
    1.16 - 1.18.

    Run `kubectl version --short` to see the versions:

        $ kubectl version --short
        Client Version: v1.17.3
        Server Version: v1.16.8

    Kubernetes has a fast upgrade cycle, and we must stay up to date. NERSC
    updates Kubernetes on the server to a new minor version (i.e The `1.x`
    version) at least twice per year. For example, during 2021 we will upgrade
    from 1.17 to 1.18 to 1.19 to stay on a supported version.

!!! important "Keep these tools up to date"
    The `rancher` & `kubectl` CLIs are updated at least once per
    quarter. You are responsible for keeping the software up to date on your
    own system.

### Generate the API credentials & login

!!! danger "Security Note"
    These API keys give full control to everything within your
    Project & Namespaces. Guard them well.

!!! notice
    The UI will only provide one opportunity to copy the credentials, and you
    will not be able to retrieve the credentials if you close the window.
    However, you can always delete and recreate the credentials.

You will need:

1. A terminal window which is logged into a NERSC login node, or a terminal
   window open on your laptop
1. A working Rancher CLI

First, we will generate the API Key (token):

1. Login to https://rancher2.spin.nersc.gov/ 
1. Browse to the API credential menu at **User Icon** (Upper right) -> **Account and API &
   Keys**
1. Create a key with the following settings

    * Automatically Expire -> 1 year from now. If you're just testing, set
      the expiration to be 1 month from now. (Expiration dates are *good for
      security*)
    * Name = Whatever (Pro Tip: Put the expiration date in the name)
    * Leave *Scope* as "no scope" (Important)
    * Hit the *Create* button
    * You are now on a screen that says "A new API Key has been created" 
      * *Don't click Done until you have saved the key*
      * Make note of the important fields `Endpoint:` and `Bearer Token:`
        * Ignore the fields `Access Key (username)` and `Secret Key (password)`.
          They are combined into the `Bearer Token`.
    * Keep the browser window open for now

Now, head over to your terminal window, and use the API Key to log into
Rancher. Use the values of the *Endpoint* (Including the `/v3` at the end) and
*Bearer Token* (including the part before and after the `:`), like so:

    rancher login --token <Bearer Token> <Endpoint URL>

You will see something similar to this:

    stefanl@spin:~ $ rancher login --token a1b2c3:AAAABBBBCCCCDDDDEEEE https://rancher2.spin.nersc.gov/v3
    INFO[0000] Only 1 project available: spinup
    INFO[0000] Saving config to /global/homes/s/stefanl/.rancher/cli2.json
    stefanl@spin:~ $

And now, test that your token is valid. You will see a list of projects that you have access too:

    stefanl@spin:~ $ rancher project
    ID                NAME      STATE     DESCRIPTION
    c-fwj56:p-6q5dd   spinup    active
    stefanl@spin:~ $

## Rancher CLI highlights

The `rancher` CLI supports Rancher-specific commands as well as
Kubernetes-specific subcommands via `rancher kubectl`. Each style of commands
has its own help:

* `rancher --help` for help with the Rancher-specific commands
* `rancher kubectl --help` for help with the Kubernetes-specific commands.

Some examples are:

* `rancher ps` - Show workloads in projects
* `rancher namespaces` - Create/remove/move namespaces within a project

See other Rancher CLI examples at https://rancher.com/docs/rancher/v2.x/en/cli/

Kubernetes examples would be:

* `rancher kubectl apply -f my-deployment.yml` - Update a Kubernetes resource
   with the configuration specified in `my-deployment.yaml`.
* `rancher kubectl annotate ...` - Apply an annotation to a Kubernetes resource

See other Kubectl examples at
https://kubernetes.io/docs/reference/kubectl/overview/ . Remember these must
all called as `rancher kubectl`.

## Advanced

### Multiple projects

If you have multiple projects (also called "contexts"), Rancher will prompt you to select a `context`:

    stefanl@spin:~ $ rancher login https://rancher2.spin.nersc.gov/v3 --token token-lsw5b:123468734878473897189789127389127
    NUMBER    CLUSTER NAME   PROJECT ID        PROJECT NAME   PROJECT DESCRIPTION
    1         development    c-fwj56:p-2tlth   stefanl        
    2         development    c-fwj56:p-4qjxn   spintest       
    Select a Project:

### More than one token? Use --name

    stefanl@spin:~ $ rancher login --name foo --token token-nql59:lkasjdklsljaskldjaskljasdljsa https://rancher2.spin.nersc.gov/v3
    INFO[0000] Only 1 project available: spinup
    INFO[0000] Saving config to /global/homes/s/stefanl/.rancher/cli2.json
    stefanl@spin:~ $

#### To see multiple tokens

    stefanl@spin:~ $ cat /global/homes/s/stefanl/.rancher/cli2.json  | jq
    {
      "Servers": {
          "foo": {
          "accessKey": "token-EFG59",
          "secretKey": "lkasjdklsljaskldjaskljasdljsa",
          "tokenKey": "token-nql59:lkasjdklsljaskldjaskljasdljsa",
          "url": "https://rancher2.spin.nersc.gov",
          "project": "c-fwj56:p-6q5dd",
          "cacert": ""
        },
        "rancherDefault": {
          "accessKey": "token-ABC79",
          "secretKey": "lkasjdklsljaskldjaskljasdljsa",
          "tokenKey": "token-nql59:lkasjdklsljaskldjaskljasdljsa",
          "url": "https://rancher2.spin.nersc.gov",
          "project": "c-fwj56:p-6q5dd",
          "cacert": ""
        }
      },
      "CurrentServer": "foo"
    }
    stefanl@spin:~ $
