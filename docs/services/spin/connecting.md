# Connecting to a Spin App

In order to be able to access your application from outside of Spin (for
example from a web browser, over the Internet) you must tell Spin how to route
incoming requests. This can be done by configuring an Ingress. It is similar to
the configuration used by a web server, which translates URLs to file system
paths.

**Important:** before creating an ingress, make sure that you have created a `ClusterID` type port in your deployment.

## HTTP-based services: Ingresses

If you want users to access an endpoint that speaks the `HTTP` protocol (ie. a
web server, or an API), go to the `Service Discovery` in the left menu, select `Ingresses`.
Click the `Create` button on the upper left. Select the correct namespace, and give your ingress
a name.

On the `Rules` tab, for the `Requested Host` entry, enter the following (substitute your
information as needed):

```
<name>.<namespace>.<environment>.svc.spin.nersc.org
```

Where:

- name can be anything (except contain a period)

- namespace is the selected namespace

- environment is the current one ("production" or "development")

- Note that the domain ends in ".org" not ".gov"

Next, set `path` to "implementation specific", and for `Target Service` select the HTTP-based workload you want to access and
enter a port. If no workloads show up under `Target Service` it is likely because you didn't declare a ClusterID
port in the workload.

In the `Ingress Class` tab, select the nginx as the ingress class.

Click "Create" and wait for 1-5 mins for DNS to propagate. Then try accessing your app at

```
http://<name>.<namespace>.<environment>.svc.spin.nersc.org:<port>
```

### HTTPS access and Custom DNS names

DOE security requirements dictate that web services must be encrypted using
a web certificate. Generally, obtaining a certificate will be done in
conjunction with obtaining a custom DNS name for your app. Processes for
obtaining certificates and registering DNS names vary from institution
to institution, so check with your local IT support or project collaborators
for guidance.

Your custom DNS name should simply be a `CNAME` record to the long, generated
name above ending in `svc.spin.nersc.org`; for example,

```
<custom DNS name> CNAME <name>.<namespace>.<environment>.svc.spin.nersc.org
```

Once you have a web certificate and custom DNS name, edit the ingress
and add a rule to specify the DNS name to use and select the same port
and target as the ingress. You should now be able to access your application via:

```
http://<custom DNS name>
```

To install the web certificate, click "Storage",
select "Secrets" and click the "Create" button. Click "TLS Certificate" and
enter the private key and the certificate. Next go to "Service Discovery" and then "Ingresses".
Find your ingress, click its name and from the right-hand side `⋮` menu, select `Edit Config`.
Click the `Certificates` tab and then `Add Certificate`. Select your previously created certificate and add
the associated host names. You should now be able to access your application at:

```
https://<custom DNS name>
```

## Non-HTTP services: Load Balancers

Services such as MySQL, PostgreSQL and other non-HTTP applications can also be
opened to other networks at NERSC. Note that access to these applications are
restricted to NERSC networks only, although we are working on a method to
expose these services to other facilities.

For this example, let's assume that you have set up a `mysql` workload called
`db` and want to connect to it from the Cori or Perlmutter login nodes.

Click `Edit Config` on your `db` workload, and add then `Add Port` as follows:

* Service type: Load Balancer
* Name: mysql
* Port: 3306
* Protocol: TCP

Notice that your workload now has loadbalancer Service called `db-loadbalancer`.
The service will have an automatically generated
name in the format `<workload>-loadbalancer`. Spin will automatically generate
the DNS hostname for your service, in the following format:

```
<workload>-loadbalancer.<namespace>.<environment>.svc.spin.nersc.org
```

Click `⋮` > `Edit Config` on the `db-loadbalancer` line, and click on the `Labels and Annotations` tab.
Notice that the hostname for your service is automatically set with the following annotation:

```
external-dns.alpha.kubernetes.io/hostname=<name>.<namespace>.<environment>.svc.spin.nersc.org
```

This is the hostname to use for accessing your application. Click cancel to exit this view.

Optionally, from your deployment,  click the `Services` tab and notice there are two
Services: `db` that exposes a port within your namespace, and `db-loadbalancer`
which is the same service that you saw under the `Load Balancing` tab.

!!! warning "Ports allowed for non-HTTP services"
    Only the following ports can be accessed when using non-HTTP services: 873, 3128, 3306, 4873, 5432, 5672, 5984, 27017, 15672
    These ports cover standard ports for databases and popular network services.

### External Traffic Policy

A feature called `External Traffic Policy` controls how traffic from sources
outside of Spin is routed to your application. There are two choices:

* The `Local` policy is is faster then `Cluster`, as your loadbalancer endpoint
  will reside on the same node as your service. It will also preserve the
  client source IP for applications that need it.
* The `Cluster` policy provides better network redundancy, but at the expense
  of one network hop which reduces performance. It also obscures the client
  source IP.

For most services, we recommend that you set this to `Local`.  To do this, on
your loadbalancer service, click on `⋮` > `Edit`.  Scroll down to `External
Traffic Policy` and select `Local`.

To connect to your external hostname, use the above hostname and the
application port. For example for a hypothetical mysql workload:

```
mysqlsh -h db-loadbalancer.<namespace>.<environment>.svc.spin.nersc.org -u root
```
