#!/bin/bash
#SBATCH -J test_cr
#SBATCH -q flex
#SBATCH -N 1 
#SBATCH -C knl
#SBATCH -t 48:00:00
#SBATCH -o %x-%j.out
#SBATCH -e %x-%j.err
#SBATCH --time-min=2:00:00

#user setting
export OMP_PROC_BIND=spread
export OMP_PLACES=threads
export OMP_NUM_THREADS=64

#to c/r with dmtcp
module load dmtcp nersc_cr

#checkpointing once every hour
start_coordinator -i 3600

#running under dmtcp control
dmtcp_launch -j ./a.out

